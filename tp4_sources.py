#Ex 1
def plus_long_plateau(chaine):
    """recherche la longueur du plus grand plateau d'une chaine

    Args:
        chaine (str): une chaine de caractères

    Returns:
        int: la longueur de la plus grande suite de lettres consécutives égales
    """    
    lg_max=0 # longueur du plus grand plateau déjà trouvé
    lg_actuelle=0 #longueur du plateau actuel
    for i in range(len(chaine)):
        if chaine[i] == chaine[i-1]: # si la lettre actuelle est égale à la précédente
            lg_actuelle+=1
        else: # si la lettre actuelle est différente de la précédente
            if lg_actuelle>lg_max:
                lg_max=lg_actuelle
            lg_actuelle=1
    if lg_actuelle>lg_max: #cas du dernier plateau
        lg_max=lg_actuelle
    return lg_max

def test_plus_long_plateau():
    assert plus_long_plateau("colle") == 2
    assert plus_long_plateau("parapluie") == 1
    assert plus_long_plateau("comme") == 2
    assert plus_long_plateau("tribunal") == 1


#Ex 2
# --------------------------------------
# Exemple de villes avec leur population
# --------------------------------------
liste_villes=["Blois","Bourges","Chartres","Châteauroux","Dreux","Joué-lès-Tours","Olivet","Orléans","Tours","Vierzon"]
population=[45871, 64668,  38426, 43442, 30664, 38250, 22168, 116238, 136463,  25725]

def max_hab(liste_villes, population):
    """recherche la ville avec le plus d'habitants

    Args:
        liste_villes (list): liste de villes
        population (list): populations de chaque villes
    Returns:
        str : retourne la ville avec le plus d'habitants
    """
    compteur = -1
    max = 0
    for nb in population :
        compteur += 1
        if nb > max :
            max = nb
            ctemp = compteur
    return liste_villes[ctemp]

def test_max_hab():
    assert max_hab(["Blois","Bourges","Chartres","Châteauroux","Dreux","Joué-lès-Tours","Olivet","Orléans","Tours","Vierzon"],[45871, 64668,  38426, 43442, 30664, 38250, 22168, 116238, 136463,  25725]
) == "Tours"
    assert max_hab(["Toulouse","Bordeaux"],[472000, 249000]) == "Toulouse"
    assert max_hab(["saint_marceau", "beaumont", "vivoin", "teillé"],[513,1200,650,610]) == "beaumont"
    assert max_hab(["lemans", "oisseau", "assé", "juillet"],[100000,1000,680,460])

#Ex3

def transfo(nombre):
    """transforme str en nombre

    Args:
        nombre (str): nombre en chaine de carac

    Returns:
        int : le nombre de la chaine de carac
    """
    compteur = -1
    res = 0
    verif = "0123456789"
    liste_nb = []
    liste_inverse = []

    for nb in nombre:
        for v in verif:
            compteur += 1
            if nb == v :
                liste_nb.append(compteur)
        compteur = -1

    elem = len(liste_nb)-1

    while elem >= 0 :
        liste_inverse.append(liste_nb[elem])
        elem -= 1

    cpt = -1

    for chiffre in liste_inverse :
        cpt +=1
        res += (chiffre*(10**cpt))
    return res

print(transfo("2021"))

def test_transfo():
    assert transfo("2021") == 2021
    assert transfo("155330") == 155330
    assert transfo("0") == 0
    assert transfo("465978") == 465978

#Ex4

def mot(liste,lettre):
    """recherche les mots d'une liste commancant par une lettre choisie

    Args:
        liste (list): liste de mots
        lettre (str): lettre voulue

    Returns:
        res : liste de mots 
    """
    res = []
    for mot in liste :
        if lettre.lower() == mot[0].lower():
            res.append(mot)
    return res

def test_mot():
    assert mot(["bonjour","carotte","Barnabé"],'b') == ['bonjour','Barnabé']
    assert mot([],'c') == []
    assert mot(["Marguerite", "lucienne", "barbara", "Leon"],'y') == []
    assert mot(["addolphe","franz","Heinrich","kärl","Markus"],'') == []
    
# Ex5

def decoupage(chaine):
    """transforme une str en liste de mots

    Args:
        chaine (str): texte que l'on veut transfo

    Returns:
        res: la liste de mots
    """
    res = []
    liste = []
    for lettre in chaine :
        if lettre.isalpha() == True:
            liste.append(lettre)
        else :
            mot = "".join(liste)
            if mot!='':
                res.append(mot)
                liste = []
    return res

def test_decoupage():
    assert decoupage("Cela fait déjà 28 jours! 28 jours à l’IUT’O! Cool ") == ['Cela','fait','déjà','jours','jours','à','l','IUT','O','Cool']
    assert decoupage("") == []
    assert decoupage("    ") == []
    assert decoupage("=== salut ===") == ['salut']

# Ex6

def recherche_mot(str,lettre):
    """recherche les mots d'un texte commencant par la lettre choisie

    Args:
        str (str): texte que l'on veut analyser
        lettre ([type]): [description]

    Returns:
        res: les mots commencant par la lettre choisie
    """
    str += ' '
    r = decoupage(str)
    s = mot(r,lettre)
    return s

def test_recherche_mot():
    assert recherche_mot("Cela fait déjà 28 jours! 28 jours à l’IUT’O! Cool",'c') == ['Cela','Cool']
    assert recherche_mot("",'a') == []
    assert recherche_mot("blanc noir rouge",'') == []
    assert recherche_mot("=== salut ===",'s') == ['salut']

#Ex 7